
FactoryGirl.define do
  factory :developer do
    email 'joe@example.com'
    password 'changeme'
  end

  factory :game do
    name 'Super Jump Bros'
    description 'They jump so high!!'
    studio_id 1
  end

  factory :player do
    handle 'Svarthol'
    email 'svarthol@example.org'
    password 'weaksauce'
  end
end


