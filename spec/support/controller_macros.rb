module ControllerMacros
  def login_developer
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:developer]
      sign_in FactoryGirl.create(:developer)
    end
  end

  def login_player
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:player]
      sign_in FactoryGirl.create(:player)
    end
  end
end
