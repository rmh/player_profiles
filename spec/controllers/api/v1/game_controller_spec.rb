require 'rails_helper'

describe Api::V1::GameController do
  context 'While authenticating' do
    it 'rejects requests without a game_id' do
      get :index

      expect(response.status).to eql 401
    end

    it 'rejects requests without an auth_token' do
      g = FactoryGirl.create(:game)
      get :index, {game_id: g.game_id}

      expect(response.status).to eql 401
    end

    it 'rejects requests without a valid auth_token' do
      g = FactoryGirl.create(:game)
      get :index, {game_id: g.game_id, auth_token: 'bananas!'}

      expect(response.status).to eql 401
    end

    it 'authorizes requests with a valid game_id & auth_token' do
      g = FactoryGirl.create(:game,
                            auth_token: 'decafbad')

      get :index, {game_id: g.game_id, auth_token: g.auth_token}

      expect(response.status).to eql 200
    end
  end

  context 'While registering a player' do
    it 'will attempt to find a player by handle' do
      game = FactoryGirl.create(:game,
                                 auth_token: 'decafbad')
      player = FactoryGirl.create(:player)

      get :register_player, {game_id: game.game_id, auth_token: game.auth_token, handle: player.handle}

      expect(assigns(:player)).to eq(player)
    end

    it 'will attempt to find a player by email' do
      game = FactoryGirl.create(:game,
                                 auth_token: 'decafbad')
      player = FactoryGirl.create(:player)

      get :register_player, {game_id: game.game_id, auth_token: game.auth_token, email: player.email}

      expect(assigns(:player)).to eq(player)
    end

    it 'will fail if the player is already registered' do
      game = FactoryGirl.create(:game,
                                 auth_token: 'decafbad')
      player = FactoryGirl.create(:player)

      get :register_player, {game_id: game.game_id, auth_token: game.auth_token, handle: player.handle}
      get :register_player, {game_id: game.game_id, auth_token: game.auth_token, handle: player.handle}

      expect(response.status).to eql 409
    end
  end
end

