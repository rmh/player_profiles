require 'rails_helper'

describe Game, :type => :model do
  context 'when creating a new Game' do
    subject { Game.new(name: 'Super Jump Brothers',
                       description: 'They jump so high! No respect for gravity what so ever...',
                       studio_id: 1) }
    it 'will create a game_id hash' do
      expect(subject.game_id).to be_nil

      subject.save!

      expect(subject.game_id).to_not be_nil
    end
  end

  context 'When updating a game' do
    subject { Game.create(name: 'jump Brothers',
                          description: 'They jump so high! No respect for gravity what so ever...',
                          studio_id: 1) }
    it 'will not override the game_id' do
      original_id = subject.game_id
      subject.name = 'Super Jump Brothers'
      subject.save!

      expect(subject.game_id).to eql original_id
    end
  end
end
