require "rails_helper"

RSpec.describe AchievementsController, :type => :routing do
  describe "routing" do
    let(:game) { FactoryGirl.create(:game) }

    it "routes to #index" do
      expect(:get => "/games/#{game.game_id}/achievements").to route_to("achievements#index", game_id: game.game_id)
    end

    it "routes to #new" do
      expect(:get => "/games/#{game.game_id}/achievements/new").to route_to("achievements#new", game_id: game.game_id)
    end

    it "routes to #show" do
      expect(:get => "/games/#{game.game_id}/achievements/1").to route_to("achievements#show", :id => "1", game_id: game.game_id)
    end

    it "routes to #edit" do
      expect(:get => "/games/#{game.game_id}/achievements/1/edit").to route_to("achievements#edit", :id => "1", game_id: game.game_id)
    end

    it "routes to #create" do
      expect(:post => "/games/#{game.game_id}/achievements").to route_to("achievements#create", game_id: game.game_id)
    end

    it "routes to #update" do
      expect(:put => "/games/#{game.game_id}/achievements/1").to route_to("achievements#update", :id => "1", game_id: game.game_id)
    end

    it "routes to #destroy" do
      expect(:delete => "/games/#{game.game_id}/achievements/1").to route_to("achievements#destroy", :id => "1", game_id: game.game_id)
    end

  end
end
