require 'rails_helper'

RSpec.describe "studios/index", :type => :view do
  before(:each) do
    assign(:studios, [
      Studio.create!(
        :name => "Name",
        :description => "Description",
        :poc_name => "Poc Name",
        :poc_email => "Poc Email",
        :poc_phone => "Poc Phone",
        :address => "Address",
        :secondary_address => "Secondary Address",
        :state_province => "State Province",
        :country => "Country",
        :zip_postal => "Zip Postal"
      ),
      Studio.create!(
        :name => "Name",
        :description => "Description",
        :poc_name => "Poc Name",
        :poc_email => "Poc Email",
        :poc_phone => "Poc Phone",
        :address => "Address",
        :secondary_address => "Secondary Address",
        :state_province => "State Province",
        :country => "Country",
        :zip_postal => "Zip Postal"
      )
    ])
  end

  it "renders a list of studios" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Poc Name".to_s, :count => 2
    assert_select "tr>td", :text => "Poc Email".to_s, :count => 2
    assert_select "tr>td", :text => "Poc Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Secondary Address".to_s, :count => 2
    assert_select "tr>td", :text => "State Province".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Zip Postal".to_s, :count => 2
  end
end
