require 'rails_helper'

RSpec.describe "studios/show", :type => :view do
  before(:each) do
    @studio = assign(:studio, Studio.create!(
      :name => "Name",
      :description => "Description",
      :poc_name => "Poc Name",
      :poc_email => "Poc Email",
      :poc_phone => "Poc Phone",
      :address => "Address",
      :secondary_address => "Secondary Address",
      :state_province => "State Province",
      :country => "Country",
      :zip_postal => "Zip Postal"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/Poc Name/)
    expect(rendered).to match(/Poc Email/)
    expect(rendered).to match(/Poc Phone/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Secondary Address/)
    expect(rendered).to match(/State Province/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Zip Postal/)
  end
end
