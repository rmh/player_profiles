require 'rails_helper'

RSpec.describe "studios/edit", :type => :view do
  before(:each) do
    @studio = assign(:studio, Studio.create!(
      :name => "MyString",
      :description => "MyString",
      :poc_name => "MyString",
      :poc_email => "MyString",
      :poc_phone => "MyString",
      :address => "MyString",
      :secondary_address => "MyString",
      :state_province => "MyString",
      :country => "MyString",
      :zip_postal => "MyString"
    ))
  end

  it "renders the edit studio form" do
    render

    assert_select "form[action=?][method=?]", studio_path(@studio), "post" do

      assert_select "input#studio_name[name=?]", "studio[name]"

      assert_select "input#studio_poc_name[name=?]", "studio[poc_name]"

      assert_select "input#studio_poc_email[name=?]", "studio[poc_email]"

      assert_select "input#studio_poc_phone[name=?]", "studio[poc_phone]"

      assert_select "input#studio_address[name=?]", "studio[address]"

      assert_select "input#studio_secondary_address[name=?]", "studio[secondary_address]"

      assert_select "input#studio_state_province[name=?]", "studio[state_province]"

      assert_select "input#studio_country[name=?]", "studio[country]"

      assert_select "input#studio_zip_postal[name=?]", "studio[zip_postal]"
    end
  end
end
