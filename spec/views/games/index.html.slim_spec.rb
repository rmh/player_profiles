require 'rails_helper'

RSpec.describe "games/index", :type => :view do
  before(:each) do
    assign(:games, [
      Game.create!(
        :name => "Name",
        :description => "Description",
        :cover_image_loc => "Cover Image Loc",
        :studio => nil,
        :game_id => "Game",
        :studio_id => 1,
        :auth_token => "Auth Token"
      ),
      Game.create!(
        :name => "Name",
        :description => "Description",
        :cover_image_loc => "Cover Image Loc",
        :studio => nil,
        :game_id => "Game",
        :studio_id => 1,
        :auth_token => "Auth Token"
      )
    ])
  end

  it "renders a list of games" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Cover Image Loc".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Game".to_s, :count => 2
    assert_select "tr>td", :text => "Auth Token".to_s, :count => 2
  end
end
