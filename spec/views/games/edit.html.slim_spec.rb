require 'rails_helper'

RSpec.describe "games/edit", :type => :view do
  before(:each) do
    @game = assign(:game, Game.create!(
      :name => "MyString",
      :description => "MyString",
      :cover_image_loc => "MyString",
      :game_id => "decafbad",
      :studio_id => 1,
      :auth_token => "MyString"
    ))
  end

  it "renders the edit game form" do
    render

    assert_select "form[action=?][method=?]", game_path(@game), "post" do

      assert_select "input#game_name[name=?]", "game[name]"

      assert_select "input#game_description[name=?]", "game[description]"

      assert_select "input#game_cover_image_loc[name=?]", "game[cover_image_loc]"

      assert_select "input#game_studio[name=?]", "game[studio]"

      assert_select "input#game_auth_token[name=?]", "game[auth_token]"
    end
  end
end
