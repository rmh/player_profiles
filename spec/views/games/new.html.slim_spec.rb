require 'rails_helper'

RSpec.describe "games/new", :type => :view do
  before(:each) do
    assign(:game, Game.new(
      :name => "MyString",
      :description => "MyString",
      :cover_image_loc => "MyString",
      :studio_id => 'decafbad',
      :game_id => "MyString",
      :auth_token => "MyString"
    ))
  end

  it "renders new game form" do
    render

    assert_select "form[action=?][method=?]", games_path, "post" do

      assert_select "input#game_name[name=?]", "game[name]"

      assert_select "input#game_description[name=?]", "game[description]"

      assert_select "input#game_cover_image_loc[name=?]", "game[cover_image_loc]"

      assert_select "input#game_studio[name=?]", "game[studio]"

      assert_select "input#game_auth_token[name=?]", "game[auth_token]"
    end
  end
end
