require 'rails_helper'

RSpec.describe "games/show", :type => :view do
  before(:each) do
    @game = assign(:game, Game.create!(
      :name => "Name",
      :description => "Description",
      :cover_image_loc => "Cover Image Loc",
      :studio_id => 1,
      :game_id => "decafbad",
      :auth_token => "Auth Token"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/Cover Image Loc/)
    expect(rendered).to match(//)
    expect(rendered).to match(/decafbad/)
    expect(rendered).to match(/Auth Token/)
  end
end
