Rails.application.routes.draw do

  resources :games do
    resources :achievements
  end

  namespace :api do
    namespace :v1 do
      resources :game do
        get :register_player
      end
    end
  end

  resources :studios

  devise_for :players
  devise_for :developers

  resources :stats, only: [:show]

  root to: 'home#index'

  get 'developers', to: 'home#developers'
end
