# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140707125525) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievements", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "icon_loc",    default: "images/star.png"
    t.integer  "point_value"
    t.integer  "game_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "achievements", ["game_id"], name: "index_achievements_on_game_id", using: :btree

  create_table "achievements_players", id: false, force: true do |t|
    t.integer  "achievement_id", null: false
    t.integer  "player_id",      null: false
    t.datetime "achieved_at",    null: false
  end

  add_index "achievements_players", ["achievement_id", "player_id"], name: "index_achievements_players_on_achievement_id_and_player_id", using: :btree
  add_index "achievements_players", ["player_id", "achievement_id"], name: "index_achievements_players_on_player_id_and_achievement_id", using: :btree

  create_table "developers", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "name"
  end

  add_index "developers", ["email"], name: "index_developers_on_email", unique: true, using: :btree
  add_index "developers", ["reset_password_token"], name: "index_developers_on_reset_password_token", unique: true, using: :btree

  create_table "developers_studios", id: false, force: true do |t|
    t.integer "developer_id", null: false
    t.integer "studio_id",    null: false
    t.integer "role",         null: false
  end

  add_index "developers_studios", ["developer_id", "studio_id"], name: "index_developers_studios_on_developer_id_and_studio_id", using: :btree
  add_index "developers_studios", ["studio_id", "developer_id"], name: "index_developers_studios_on_studio_id_and_developer_id", using: :btree

  create_table "game_stats", force: true do |t|
    t.json     "statistics"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "games", force: true do |t|
    t.string   "name",                       null: false
    t.text     "description",                null: false
    t.string   "cover_image_loc"
    t.integer  "studio_id",                  null: false
    t.string   "game_id",         limit: 40, null: false
    t.string   "auth_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "game_stats"
  end

  add_index "games", ["studio_id"], name: "index_games_on_studio_id", using: :btree

  create_table "games_players", id: false, force: true do |t|
    t.integer "game_id",   null: false
    t.integer "player_id", null: false
  end

  add_index "games_players", ["game_id", "player_id"], name: "index_games_players_on_game_id_and_player_id", using: :btree
  add_index "games_players", ["player_id", "game_id"], name: "index_games_players_on_player_id_and_game_id", using: :btree

  create_table "players", force: true do |t|
    t.string   "handle",                              null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "avatar_location"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "global_stats",           default: {}
  end

  add_index "players", ["email"], name: "index_players_on_email", unique: true, using: :btree
  add_index "players", ["handle"], name: "index_players_on_handle", unique: true, using: :btree
  add_index "players", ["reset_password_token"], name: "index_players_on_reset_password_token", unique: true, using: :btree

  create_table "studios", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "poc_name"
    t.string   "poc_email"
    t.string   "poc_phone"
    t.string   "address"
    t.string   "secondary_address"
    t.string   "state_province"
    t.string   "country"
    t.string   "zip_postal"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
