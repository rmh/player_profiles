class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name, null: false
      t.text   :description, null: false
      t.string :cover_image_loc
      t.references :studio, index: true, null: false
      t.string :game_id, null: false, index: true, limit: 40
      t.string :auth_token

      t.timestamps
    end
  end
end
