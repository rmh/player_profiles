class CreateJoinTableAchievementsPlayer < ActiveRecord::Migration
  def change
    create_join_table :achievements, :players do |t|
      t.index [:achievement_id, :player_id]
      t.index [:player_id, :achievement_id]
      t.timestamp :achieved_at, null: false
    end
  end
end
