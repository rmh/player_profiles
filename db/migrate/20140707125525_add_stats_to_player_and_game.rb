class AddStatsToPlayerAndGame < ActiveRecord::Migration
  def change
    add_column :players, :global_stats, :json, default: '{}'

    create_table :game_stats do |t|
      t.json :statistics

      t.timestamps
    end

    add_column :games, :game_stats, :integer
  end
end
