class AddDefaultAchievementIcon < ActiveRecord::Migration
  def change
    change_column :achievements, :icon_loc, :string, default: 'images/star.png'
  end
end
