class CreateStudios < ActiveRecord::Migration
  def change
    create_table :studios do |t|
      t.string :name
      t.string :description
      t.string :poc_name
      t.string :poc_email
      t.string :poc_phone
      t.string :address
      t.string :secondary_address
      t.string :state_province
      t.string :country
      t.string :zip_postal

      t.timestamps
    end
  end
end
