class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements do |t|
      t.string :name
      t.string :description
      t.string :icon_loc
      t.integer :point_value
      t.references :game, index: true

      t.timestamps
    end
  end
end
