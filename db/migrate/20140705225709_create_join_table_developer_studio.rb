class CreateJoinTableDeveloperStudio < ActiveRecord::Migration
  def change
    create_join_table :developers, :studios do |t|
      t.index [:developer_id, :studio_id]
      t.index [:studio_id, :developer_id]
      t.integer :role, null: false
    end
  end
end
