class AutoCreateAchievementTime < ActiveRecord::Migration
  def change
    execute <<-SQL
      ALTER TABLE achievements_players
        ALTER COLUMN achieved_at SET DEFAULT statement_timestamp();
    SQL
  end
end
