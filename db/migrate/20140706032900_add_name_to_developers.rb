class AddNameToDevelopers < ActiveRecord::Migration
  def change
    add_column :developers, :name, :text
  end
end
