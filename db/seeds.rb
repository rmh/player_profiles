# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#
if ENV['MOCK_DATA']
  a_studio = Studio.create(name: 'Alpha Centauri Games')
  b_studio = Studio.create(name: 'Big Ideas')
  c_studio = Studio.create(name: 'Cranky Goat Games')

  dd = Developer.create(name: 'Donald Duck', email: 'dd@example.com', password: 'lisp_fan')
  mm = Developer.create(name: 'Micky Mouse', email: 'mm@example.com', password: 'clojure_conosure')

  ActiveRecord::Base.connection.execute("INSERT INTO developers_studios (developer_id, studio_id, role) VALUES
                                          (#{dd.id}, #{a_studio.id}, 0),
                                          (#{dd.id}, #{b_studio.id}, 0),
                                          (#{mm.id}, #{b_studio.id}, 1),
                                          (#{mm.id}, #{c_studio.id}, 0)")

  Game.create(name: 'Super Jump Brothers',
              description: 'They jump so high, no regard for the laws of physics...',
              studio_id: c_studio.id)
end
