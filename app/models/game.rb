class Game < ActiveRecord::Base
  belongs_to :studio
  has_and_belongs_to_many :players
  after_validation :build_game_id
  after_create :add_stats

  # We don't want this to change as it is the way games are associated with users.
  # We'll use this instead of the primary key for security reasons.
  def build_game_id
    if !game_id
      self.game_id = Digest::SHA1.hexdigest("#{studio_id}_#{id}_#{name}")
    end
  end

  def add_stats
    if !self.game_stats
      stats = GameStat.new
      stats.save

      self.game_stats = stats.id
      self.save!
    end
  end
end
