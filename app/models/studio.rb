class Studio < ActiveRecord::Base
  # The role enumerates the role column of the Studio/Developer Join Table
  # it dictates what permissions a given user has with the studio.
  enum role: [:admin, :developer]

  has_and_belongs_to_many :developers
end
