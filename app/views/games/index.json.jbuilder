json.array!(@games) do |game|
  json.extract! game, :id, :name, :description, :cover_image_loc, :studio_id, :game_id, :auth_token
  json.url game_url(game, format: :json)
end
