json.array!(@studios) do |studio|
  json.extract! studio, :id, :name, :description, :poc_name, :poc_email, :poc_phone, :address, :secondary_address, :state_province, :country, :zip_postal
  json.url studio_url(studio, format: :json)
end
