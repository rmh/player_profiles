json.extract! @studio, :id, :name, :description, :poc_name, :poc_email, :poc_phone, :address, :secondary_address, :state_province, :country, :zip_postal, :created_at, :updated_at
