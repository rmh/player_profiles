json.array!(@achievements) do |achievement|
  json.extract! achievement, :id, :name, :description, :icon_loc, :point_value, :game_id
  json.url achievement_url(achievement, format: :json)
end
