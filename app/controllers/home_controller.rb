
class HomeController < ApplicationController
  def index
    debugger
    if current_player
      redirect_to "/stats/#{current_player.handle}"
    elsif current_developer
      redirect_to '/developers'
    end
  end

  def developers
  end
end
