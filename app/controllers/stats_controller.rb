class StatsController < ApplicationController
  # GET /stats/1
  # GET /stats/1.json
  def show
    @player = Player.where('lower(handle) = ?', params[:id].downcase).first

    unless @player
      raise ActionController::RoutingError.new("Player #{params[:handle]} Not Found")
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def stat_params
      params[:stat]
    end
end
