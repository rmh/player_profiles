module Api
  module V1
    class ApiController < ActionController::Base
      before_filter :restrict_access
      respond_to :json
      force_ssl if Rails.env == 'Production'

      private
      def restrict_access
        if params[:game_id] && params[:auth_token]
          @game = Game.find_by(game_id: params[:game_id], auth_token: params[:auth_token])

          if @game.nil?
            head :unauthorized
          end
        else
          head :unauthorized
        end
      end
    end
  end
end
