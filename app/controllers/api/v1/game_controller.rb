module Api
  module V1
    class GameController < ApiController
      def index
        render json: {}
      end

      def register_player
        # Using where clause instead of select for case-independent search
        if params[:handle]
          @player = Player.where('lower(handle) = ?', params[:handle].downcase).first
        elsif params[:email]
          @player = Player.where('lower(email) = ?', params[:email].downcase).first
        end

        if @player.nil?
          head :nor_found
        else
          @game = Game.find_by(game_id: params[:game_id])

          if @player.games.detect { |g| g.game_id == @game.game_id }
            head :conflict
          else
            @player.games << @game
            render json: {status: 200,
                          message: 'Registered Player'}
          end
        end
      end
    end
  end
end
