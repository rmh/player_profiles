# Game Profiles
This is a code sample project as part of my interview with Agora Games.

## Getting Started
This project is built using Ruby 2.1.2, Rails 4.1.4, and PostgreSQL 9.3.
While any version of Ruby 2+ can be used, PostgreSQL 9.3 is the minimum version due to my use of the JSON datatype, which is not available in 9.2 or earlier.

To setup the database the following steps must be taken:  
`createuser -d -P player_profiles`  
This will prompt for a password.  Please verify that the username & password match what is in your config/database.yml under the developement section. Depending on your installation of Postgres, you may need to run createuser under a different user account.

Next:
`rake db:create db:migrate db:seed`
This will give you a working development database with logical defaults.  Running db:seed with the env variable `MOCK_DATA` set will populate the database with some example data.

`rails s` will start the service.  Navigate to `http://127.0.0.1:3000/help/api` to learn about interacting with this application.

## Design
This application is designed to be accessed in 3 major ways.  By players, developers, and games.

### Players
Players can view their profiles, see when they unlocked achievements in their games, view global statistics (ie, total playtime, total achievement points) as well as per game statistics.

### Developers
Developers will be associated with a studio, and can add games & and achievements to the system.  They will also be able to manage other developers in their studio, given the correct permissions (they are an admin for that Studio).  Developers can be associated with multiple studios.

### Games
Games themselves will interact with the system by updating game stats & achievements.  Games will be assigned authentication keys in order to interact with our API to pevent achievement spoofing/cheating.
